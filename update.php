<?php
require_once('conexion.php');

$sql = 'SELECT * FROM news ORDER BY id DESC';

$statement = $pdo->prepare($sql);
$statement->execute();
$results = $statement->fetchAll();

$show_form = false;

if($_POST)
{
  //TODO ACTUALIZAR
  $sql_update = 'UPDATE news SET title=?,content=? WHERE id=?';
  $title=isset($_POST['title']) ? $_POST['title']: '';
  $content=isset($_POST['content']) ? $_POST['content']: '';
  $id=isset($_GET['id']) ? $_GET['id'] : 0;

  $statement_update = $pdo->prepare($sql_update);
  $statement_update->execute(array($title,$content,$id));
  
  header('Location: update.php');
}

if(isset( $_GET['id'] ) )
{
  //TODO: GET SELECCIONAR DETALLES
  $show_form = TRUE;
  $sql_detalle = 'SELECT * FROM news WHERE id=?';
  $id=isset($_GET['id']) ? $_GET['id'] : 0;

  $statement_detalle = $pdo->prepare($sql_detalle);
  $statement_detalle->execute(array($id));
  $results_detalle = $statement_detalle->fetchAll();
  $rs_detalle=$results_detalle[0];
  
  //echo '<pre>';
  //var_dump($rs_detalle);
  
}
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>PHP & SQL</title>
<link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
</head>
<body>
 
<div class="top-bar">
<div class="top-bar-left">
<ul class="menu">
<li class="menu-text"> PHP & SQL ADSI 1022830</li>
</ul>
</div>
</div>
 
<div class="row column text-center">
<h2>ACTUALIZAR</h2>
<hr>
</div>
<div class="row column">
<div class="callout primary">
<h3>Modificar Contenido</h3>
<?php 
if( $show_form )
{
?>
<form method="post">
  <div class="row">
    <div class="medium-6 columns">
      <label>Ingrese el título
        <input type="text" name="title" placeholder="ej. Javascript" value="<?php echo $rs_detalle['title'] ;?>">
        
      </label>
      <div class="help-text">El contenido deberá ser claro sobre su posible contenido</div>
    </div>
    <div class="medium-6 columns">
      <label>&nbsp;
      </label>
    </div>
  </div>
  <div class="row">
    <div class="medium-12 columns">
      <label>Ingrese el Contenido
        <textarea name="content" placeholder="ej. Lorem ipsum..."><?php echo $rs_detalle['content'] ;?></textarea>
        <input class="button primary" type="submit" value="MODIFICAR" />
      </label>
      
    </div>
  </div>
</form>
<?php } ?>
</div>
<table width="100%">
  <thead>
    <tr>
      <th>ID</th>
      <th>Título</th>
      <th>Contenido</th>
      <th>Acción</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach($results as $rs)
    {
  ?>
    <tr>
      <td width="100"><?php echo $rs['id']; ?></td>
      <td width="200"><?php echo $rs['title']; ?></td>
      <td><?php echo $rs['content']; ?></td>
      <td width="200">
        <a class="primary button" href="update.php?id=<?php echo $rs['id']; ?>">Ver detalles</a>
      </td>
    </tr>
    <?php
  }
    ?>
  </tbody>
</table>
</div>
<hr>

</div>
<div class="large-3 large-offset-2 columns">
</div>
</div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>
