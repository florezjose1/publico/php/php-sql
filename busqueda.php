<pre><?php
require_once('conexion.php');
$sql = 'SELECT * FROM news WHERE title LIKE :search ' ;
$buscar_temas =isset($_GET['title'])?$_GET['title']:'';
$arreglo_sql[':search']='%'.$buscar_temas.'%';

$statement_contar = $pdo->prepare($sql);
$statement_contar->execute($arreglo_sql);
$results_pagina =count($statement_contar->fetchAll() ) ;

$total_res_pagina =5;

$total_paginas = ceil($results_pagina/$total_res_pagina);
//echo $results_pagina;

$pagina_actual = isset($_GET['page'])?($_GET['page']):0;
$sql_parametro = $pagina_actual*$total_res_pagina;
$sql .= " LIMIT {$sql_parametro},{$total_res_pagina}";

$statement = $pdo->prepare($sql); 
$statement->execute($arreglo_sql);
$results = $statement->fetchAll();

?>
</pre>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>PHP & SQL</title>
<link rel="stylesheet" href="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.min.css">
</head>
<body>
 
<div class="top-bar">
<div class="top-bar-left">
<ul class="menu">
<li class="menu-text">ADSI 1022830 09-06-2016</li>
</ul>
</div>
</div>
 
<div class="row column text-center">
<h2>BUSQUEDA</h2>
<hr>
</div>
<div class="row column">
<div class="callout primary">
<h3>Criterio de busqueda</h3>
<form method="get">
  <div class="row">
    <div class="medium-6 columns">
      <label>Ingrese el título
        <input type="text" name="title" placeholder="ej. javascript" value="<?php echo $buscar_temas ?>">
        <input class="button" type="submit" value="BUSCAR" />
      </label>
    </div>
  </div>
</form>
</div>
<table width="100%">
  <thead>
    <tr>
      <th>Id</th>
      <th>Título</th>
      <th>Contenido</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach($results as $rs)
    {
  ?>
    <tr>
      <td><?php echo $rs['id']; ?></td>
      <td width="300"><?php echo $rs['title']; ?></td>
      <td><?php echo $rs['content']; ?></td>
    </tr>
    <?php
  }
    ?>
  </tbody>
</table>
</div>
<hr>

</div>
<div class="large-3 large-offset-2 columns">
<ul class="pagination text-center" role="navigation" aria-label="Pagination">
  
  <?php
  for ($i=1; $i <=$total_paginas ; $i++) { 
    
  ?>
  <li class="current"><a href="busqueda.php?page=<?php echo $i-1; ?>" aria-label="Page <?php echo $i;?>"><?php echo $i;?></a></li>
  <?php
}
?>
</ul>
</div>
</div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script>
      $(document).foundation();
    </script>
</body>
</html>
