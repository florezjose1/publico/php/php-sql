-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-09-2016 a las 02:01:26
-- Versión del servidor: 5.6.16
-- Versión de PHP: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `php_sql`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `publish_date` date NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `news`
--

INSERT INTO `news` (`id`, `publish_date`, `title`, `content`, `status`) VALUES
(1, '2016-01-01', 'ADSI', '<p>analisis y desarrollo de sistemas de informacion 1022830. </p>', 1),
(3, '2016-03-01', 'PHP', '<p>Tempor arcu nisl nulla, at at odio a, turpis diam vitae wisi, amet amet. Arcu pretium ridiculus erat ducimus, </p>', 1),
(4, '2016-02-19', 'JAVASCRIPT', '<p>Ultrices consequat harum, et non, tellus eu suscipit est ornare rerum quam, eros gravida odio quos sed duis commodo.</p>', 1),
(5, '2016-01-13', 'de php', '<p>Faucibus volutpat cursus sapiente inceptos sapien, urna commodo orci cursus felis nascetur.</p>', 1),
(6, '2016-02-14', 'JAVASCRIPT', '<p>ortor mi phasellus tellus mauris vehicula, et dui, cras vivamus donec vehicula convallis, quis volutpat, nunc dignissim ped</p>', 1),
(7, '2017-01-02', 'ANGULAR juan', '<p>Sit in dictum dolor quis ut urna, erat vestibulum lacus malesuada, fermentum non, nibh egestas cubilia molestie integer. Luctus et malesuada lectus vel wisi, sit nec vivamus </p>', 2),
(8, '2015-09-16', 'RUBY', '<p>Velit ultrices donec at interdum, blandit nulla maecenas justo, mauris sed, curabitur risus aliquam quis. Felis luctus felis mus dapibus lobortis, sed faucibus libero officia libero</p>', 1),
(9, '2014-12-31', 'PERL', '<p>s et scelerisque, lectus nulla voluptatem lectus in. Ipsum cras in lectus massa leo, ut interdum nascetur magna vestibulum, suspendisse eligendi consectetuer magna wisi vestibulum cras. Bibendum eleifend, lacus in, in quam suspendisse. Velit ultrices donec at interdum, blandit nulla maecenas justo, mauris sed, curabitur ri</p>', 1),
(10, '2016-07-16', 'ORACLE', '<p> consectetuer, cursus est accumsan. Fermentum iaculis diam amet, ipsum velit a enim velit, et eu, vitae ullamcorper, et cras sit tortor massa urna orci. Metus sodales rutrum ante sem, aliquam dui mollis nonummy. Orci eleifend turpis quam cum faucibus, in q</p>', 2),
(11, '2016-03-10', 'MYSQL', '<p>Faucibus volutpat cursus sapiente inceptos sapien, urna commodo orci cursus felis nascetur.</p>', 1),
(12, '2016-02-24', 'POSTGRES', '<p>Vitae vivamus cursus, in et erat sed. Turpis erat elit facilisis, faucibus eget. </p>', 1),
(13, '2015-12-31', 'VISUAL BASIC', '<p>Vestibulum placerat consectetuer, accusantium liber</p>', 1),
(14, '2016-01-01', 'JQUERY', 'Cras sodales suspendisse dui luctus enim, tempus imperdiet ridiculus.', 1),
(15, '2015-11-30', 'R ', ' empor arcu nisl nulla, at at odio a, turpis diam vitae wisi, amet amet. Arcu pretium ridiculus erat ducimus, sociis id quam mauris, ac condimentum ad, amet interdum ut varius lobortis. Sit in dictum dolor quis ut urna, erat vestibulum lacus malesuada, fermentum non, nibh egestas cubilia molestie integer. Luctus et malesuada lectus vel wisi, sit nec vivamus nulla euismod arcu, augue purus ut elit pulvinar, urna mattis suscipit justo orci ', 1),
(16, '2015-07-16', 'JQUERY', '<p>nt, leo eget viverra praesent sed mauris phasellus. Nam voluptates quam, fames sapien quis sed quam consectetuer. Mauris interdum, netus sed a gravida, nullam bla</p>', 0),
(17, '2016-06-03', 'JAVASCRIPT Ã±', 'jjjjjjjj', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_status_idx` (`status_id`),
  KEY `fk_user_user_type1_idx` (`user_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `status_id`, `user_type_id`) VALUES
(1, 'admin@correo.com', '12345678', 1, 2),
(2, 'bernardo@correo.com', '23456789', 2, 1),
(3, 'sergio@correo.com', '34567890', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_logged_in` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_log_user1_idx` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `user_log`
--

INSERT INTO `user_log` (`id`, `date_logged_in`, `user_id`) VALUES
(1, '2015-11-11', 1),
(2, '2016-03-01', 1),
(3, '2016-03-02', 2),
(4, '2016-03-03', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `user_type`
--

INSERT INTO `user_type` (`id`, `name`) VALUES
(1, 'Final'),
(2, 'Admin');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_status` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_user_type1` FOREIGN KEY (`user_type_id`) REFERENCES `user_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_log`
--
ALTER TABLE `user_log`
  ADD CONSTRAINT `fk_user_log_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
